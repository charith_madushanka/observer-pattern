﻿using System;
using System.Threading;
using ObserverPattern.Dto;
using ObserverPattern.Interfaces;

namespace ObserverPattern
{
    public class TestServer : IServer
    {
        private int currentTemperature = 36;

        private Action<Notification> notificationAction;

        public void RegisterTemperatureWatcher(Action<Notification> notification)
        {
            notificationAction += notification;
        }

        public void Start()
        {
            Console.WriteLine($"Serve started at {DateTime.Now}");

            while (currentTemperature < 60)
            {
                Thread.Sleep(1000);

                int previousTemperature = currentTemperature;
                currentTemperature++;

                if (currentTemperature > 40 && notificationAction != null)
                {
                    notificationAction(GetNotification(previousTemperature, currentTemperature));
                }
            }

            Console.WriteLine($"Server is shouting down");
        }

        private static Notification GetNotification(int previousTemperature, int currentTemperature)
        {
            return new Notification { CurrentTemperature = currentTemperature, PreviousTemperature = previousTemperature };
        }
    }
}
