﻿using ObserverPattern.Dto;
using ObserverPattern.Interfaces;
using System;

namespace ObserverPattern
{
    public class SmsSender : INotificationSender
    {
        public void Send(Notification notification)
        {
            Console.WriteLine("=====================Sending SMS=======================");
            Console.WriteLine($"Current temperature {notification.CurrentTemperature} \nPrevious temperature " +
                $"{notification.PreviousTemperature}");

            Console.WriteLine("========================================================");
        }
    }
}
