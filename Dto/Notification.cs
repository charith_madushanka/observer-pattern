﻿namespace ObserverPattern.Dto
{
    public class Notification
    {
        public int CurrentTemperature { get; set; }

        public int PreviousTemperature { get; set; }

    }
}
