﻿using ObserverPattern.Interfaces;

namespace ObserverPattern
{
    public class Program
    {
        static void Main(string[] args)
        {
            IServer server = new TestServer();
            server.RegisterTemperatureWatcher(new EmailSender().Send);
            server.RegisterTemperatureWatcher(new SmsSender().Send);
            server.Start();
        }
    }
}
