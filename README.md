

## Observer Pattern Definition 
The Observer pattern define one to many dependencies between objects so that 
one object change state, all of its dependencies are notified and updated automatically

## Participants

Subject  (IServer)
knows its observers. Any number of Observer objects may observe a Subject
provides an interface for attaching and detaching Observer objects.

ConcreteSubject  (TestServer)
stores state of interest to ConcreteObserver
sends a notification to its observers when its state changes

Observer  (ISender)
defines an updating interface for objects that should be notified of changes in a subject.
ConcreteObserver  (EmailSender,SmsSender)
maintains a reference to a ConcreteSubject object
stores state that should stay consistent with the subject's
implements the Observer updating interface to keep its state consistent with the subject's

