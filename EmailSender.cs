﻿using ObserverPattern.Dto;
using ObserverPattern.Interfaces;
using System;

namespace ObserverPattern
{
    public class EmailSender : INotificationSender
    {
        public void Send(Notification notification)
        {
            Console.WriteLine("=====================Sending email=====================");
            Console.WriteLine($"Current temperature {notification.CurrentTemperature} \nPrevious temperature " +
                $"{notification.PreviousTemperature}");

            Console.WriteLine("========================================================");
        }
    }
}
