﻿using ObserverPattern.Dto;

namespace ObserverPattern.Interfaces
{
    public interface INotificationSender
    {
        void Send(Notification notification);
    }
}
