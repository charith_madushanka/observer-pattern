﻿using ObserverPattern.Dto;
using System;

namespace ObserverPattern.Interfaces
{
    public interface IServer
    {
        void Start();

        void RegisterTemperatureWatcher(Action<Notification> notification);
    }
}
